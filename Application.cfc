component extends="core.fw1.framework" {
	
	// Application Settings
	this.name = "Blank_#hash(getCurrentTemplatePath())#";
	this.applicationTimeout = createTimespan(7,0,0,0);
	this.sessionManagement = true;
	this.sessionTimeout = createTimespan(0,0,30,0);

	include "core/config/fw1.ini.cfm"; // FW1 Settings

	// Mappings
    this.mappings['/config'] 		= expandPath("/core/config/");
    this.mappings['/model'] 		= expandPath("/model/");
	this.mappings['/views'] 		= expandPath("/views/");
	this.mappings['/services'] 		= expandPath("/services/");
	this.mappings['/controllers'] 	= expandPath("/controllers/");
	this.mappings['/layouts'] 		= expandPath("/layouts/");
	
	
	public function setupApplication() {
		application.env = getEnvironment();
		
		// Activity Logs
		writelog(text="Application #application.applicationname# started | #CGI.REMOTE_ADDR#", type="info", file="fw1");
	}

	public function setupRequest() {
	
		rc.env = application.env;

	}

	function getEnvironment() {
		
		if(findNocase("local",CGI.SERVER_NAME))
			return "dev";
		else
			return "prod";
	}

}