<cfscript>
	variables.framework = {
		action = 'action',
		// base = omitted so that the framework calculates a sensible default 
		usingSubsystems = false,
		defaultSubsystem = 'home',
		defaultSection = 'main',
		defaultItem = 'default',
		home = 'main', 
		error = 'error',
		reload = 'reload',
		password = 'true',
		reloadApplicationOnEveryRequest = true,
		baseURL = 'useCgiScriptName',
		generateSES = true,
		SESOmitIndex = true,
		applicationKey = 'core.fw1.framework',
		unhandledExtensions = 'cfc'
	};
</cfscript>