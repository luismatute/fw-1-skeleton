<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<cfoutput>
	    <!--- Include the Head --->
	    #view("includes/html_head")#
	    <body>
	        <!--- Layout Header --->
            #view("includes/head")#

	        <div id="main" role="main">
                #body# <!--- body is result of views --->
            </div>

	        <!--- Layout Footer --->
            #view("includes/footer")#

            <!--- Includes JS files --->
            #view("includes/js")#

	    </body>
	</cfoutput>
</html>