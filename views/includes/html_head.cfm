<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Blank</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!--- Because we are not robots --->
    <link type="text/plain" rel="author" href="/humans.txt">
    <link rel="icon" href="/assets/img/global/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="/assets/css/master.css">

	<!--[if lt IE 9]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>